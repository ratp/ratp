# RATP architecture

## Database format

The database format should :

- be able to store JSON-LD objects (an object is any JSON-LD value)
- be able to version these objects
- be able to manage visibility of these objects:
  * public: anyone can see the object (11)
  * owners-only : only owners of the object can access it (10)
  * internal: plugins may access the object, but federation views won't render it (and plugins shouldn't leak it) (01)
  * private: only RATP can access this object (00)
- be able to keep an object in multiple languages

Structure :

```
- db
  - languages # save of the language map
  - 0000-0000-0000-0000 (= hash of a type ID, for instance "https://schema.org#Person")
    - schema.json (the cached schema)
    - 0000-0000
      - 0000-0000
        - object.jsonld # the latest version of the object
        - history.ratph # it's history
        - owners # a list of the owners of this object, that are allowed to edit it
```

Thanks to the schema, we have a given order in which to store properties of each object.
Only basic types should be stored (Number, String, Boolean, DateTime), the other have their ID stored as pointers.

For functional values, we store them as :

```
VIS EDIT_TIME LANG VAL
```

- VIS = 2 bits (00, 01, 10, see above)
- EDIT_TIME = 64 bits (timestamp), to know if we have to look in the history or not
- LANG = 16 bits (= 65536 possible languages, stored in a common map)

For non-functional values, we always store a list, even if there is only one value.

Lists are stored as

```
SIZE_OF_1 VAL_OF_1 SIZE_OF_2 VAL_OF_2 … SIZE_OF_N VAL_OF_N
```

We have to store all the different sizes because lists may contain various types of objects.
We don't have to store the value of N, because we may now where the list ends just when we reach EOF.

Some properties may not have IDs, but they will be generated with `ID_OF_PARENT/PROP_NAME`

## Plugins

Plugins are sandboxed.

### API exposed by RATP

```
query(id: ID, langs: [String], date: Date) : Object

Object.create(schema: ID, id: ID)
Object.get(prop: String) : Result<Object, Error>
Object.set(prop: String, val: Object) : ()
Object.save() : ()
```

## Config

Example:

```toml
domain = "ap.example.org"
auto-update-delay = "1d"
auto-https = true

[users]
allow-registrations = false

[media]
auto-clean-remote = "30d"

[[app]]
name = "ratp-chat"
source = "https://github.com/ratp/ratp"
auto-update = true
path = "/chat"

[[app]]
name = "social"
source = "/srv/ratp/social"
sub-domain = "chat.example.org"
```

## CLI interface

```
ratp run
ratp install PLUGIN_URL
ratp uninstall PLUGIN_URL_OR_NAME
ratp media clean --all|--remote|--local --delay DELAY
```

## User authentication

RATP is managing user authentication. It stores the @, an email, the role (admin, moderator, normal, deactivated) and the password.A

A default plugin for creating an associated `Person` will probably be present.

This design will allow for multiple personas with one login.

Plugins will have a mechanism to show they are acting on behalf of a given account (OAuth?)

## List of default plugins

Plugins that would be nice to have installed by default (or at least officially recommended and maintained):

- `Create` handler
- `Update` handler
- `Delete` handler
- Auto-create `Person` on registration
- Administration interface
