use actix_web::{web::{Data, Path}, Responder, HttpResponse};
use indradb::{Datastore, VertexQueryExt, Transaction, RocksdbDatastore, SpecificVertexQuery, Type, Vertex};
use std::sync::Arc;
use std::collections::HashMap;
use std::str::FromStr;
use iref::{Iri, IriRef, IriBuf};
use futures_util::future::{BoxFuture, FutureExt};

pub async fn get(path: Path<String>, db: Data<Arc<RocksdbDatastore>>) -> impl Responder {
    // TODO: redirect if remote
    // TODO: check permissions (filter out fields or the whole object)
    let uuid = match uuid::Uuid::parse_str(&*path) {
        Ok(u) => u,
        Err(_) => return HttpResponse::BadRequest().body(r#"{ "error": "Invalid UUID" }"#),
    };
    let trans = match (***db).transaction() {
        Ok(t) => t,
        Err(_) => return HttpResponse::InternalServerError().body(r#"{ "error": "Database error" }"#),
    };
    let props = match trans.get_vertex_properties(SpecificVertexQuery::single(uuid).property("json")) {
        Ok(p) => p,
        Err(_) => return HttpResponse::NotFound().body(r#"{ "error": "Object not found" }"#),
    };
    let json = match props.into_iter().next() {
        Some(j) => j,
        None => return HttpResponse::NotFound().body(r#"{ "error": "Object not found" }"#),
    };
    HttpResponse::Ok()
        .header("Content-type", "application/activity+json; charset=utf-8")
        .body(json.value.to_string())
}

async fn get_act<T: serde::de::DeserializeOwned>(url: &str) -> Result<T, reqwest::Error> {
    let client = reqwest::Client::new();
    client.get(url).header("Accept", "application/activity+json")
        .send().await?
        .json().await
}

pub async fn save(db: &RocksdbDatastore, id: &str) {
    let json: serde_json::Value = get_act(id).await.unwrap();
    let t = Type::new(json["type"].as_str().unwrap().to_owned()).unwrap();
    let vert = Vertex::new(t);
    info!("saving vertex {}", vert.id);
    let trans = db.transaction().unwrap();
    trans.create_vertex(&vert).unwrap();
    trans.set_vertex_properties(SpecificVertexQuery::single(vert.id).property("json"), &json).unwrap();
}

// TODO: from string parsing ("ltr"/"rtl")
#[derive(Clone)]
enum Direction {
    Ltr,
    Rtl,
}

impl FromStr for Direction {
    type Err = ();

    fn from_str(s: &str) -> Result<Direction, ()> {
        match s {
            "rtl" => Ok(Direction::Rtl),
            "ltr" => Ok(Direction::Ltr),
            _ => Err(()),
        }
    }
}

#[derive(Clone)]
struct TermDef<'a> {
    iri: ExpandedIri,
    prefix: bool,
    protected: bool,
    reverse_property: bool,
    base_url: Option<Iri<'a>>,
    context: Option<Context<'a>>,
    container_mapping: &'a [&'a str],
    direction_mapping: Option<Direction>,
    index_mapping: Option<IriBuf>,
    language_mapping: Option<&'a str>,
    nest_value: Option<&'a str>,
    type_mapping: TypeMapping,
}

#[derive(Clone)]
enum TypeMapping {
    Id,
    Json,
    None,
    Vocab,
    Iri(IriBuf),
}

impl FromStr for TypeMapping {
    type Err = ();

    fn from_str(s: &str) -> Result<TypeMapping, ()> {
        match s {
            "@id" => Ok(TypeMapping::Id),
            "@json" => Ok(TypeMapping::Id),
            "@none" => Ok(TypeMapping::Id),
            "@vocab" => Ok(TypeMapping::Id),
            other => match IriBuf::new(other) {
                Ok(i) => Ok(TypeMapping::Iri(i)),
                Err(_) => Err(()),
            },
        }
    }
}

#[derive(Clone)]
struct Context<'a> {
    term_defs: HashMap<String, TermDef<'a>>,
    base_iri: IriBuf,
    original_base_url: Iri<'a>,
    /// Note: it might only be `ExpandedIri::Null`, `ExpandedIri::BlankNodeIdent` or
    /// `ExpandedIri::Iri`.
    vocabulary_mapping: ExpandedIri,
    default_language: Option<String>,
    default_base_direction: Option<Direction>,
    previous_context: Option<Box<Context<'a>>>,
}

enum ContextProcessingError {
    InvalidContextNullification,
    ContextOverflow,
    RemoteContextLoading(reqwest::Error),
    InvalidRemoteContext,
    InvalidLocalContext,
    InvalidVersion,
    InvalidImport,
    InvalidContextEntry,
    InvalidBaseIri,
    InvalidVocabMapping,
    InvalidDefaultLanguage,
    InvalidBaseDirection,
    InvalidPropagate,
}

impl From<reqwest::Error> for ContextProcessingError {
    fn from(err: reqwest::Error) -> ContextProcessingError {
        ContextProcessingError::RemoteContextLoading(err)
    }
}

impl<'a> Context<'a> {
    fn contains_protected_term_defintions(&self) -> bool {
        self.term_defs.iter().any(|(_, x)| x.protected)
    }
}

async fn load_document(url: &str) -> Result<serde_json::Value, reqwest::Error> {
    // TODO: implement the actual spec?
    // https://www.w3.org/TR/json-ld11-api/#dom-loaddocumentcallback
    get_act(url).await
}

const MAX_REMOTE_CONTEXTS: usize = 1024;

// https://www.w3.org/TR/json-ld11-api/#algorithm
fn context_processing<'a>(
    active_ctx: Context<'a>,
    local_ctx: serde_json::Value,
    base_url: IriBuf,
    remote_ctxs: Option<Vec<IriBuf>>,
    override_protected: Option<bool>,
    propagate: Option<bool>,
) -> BoxFuture<'a, Result<Context<'a>, ContextProcessingError>> {
    async move {
        let mut remote_ctxs = remote_ctxs.unwrap_or_default();
        let override_protected = override_protected.unwrap_or(false);
        let propagate = propagate.unwrap_or(true);

        let mut result = active_ctx.clone();
        let propagate = local_ctx["@propagate"].as_bool().unwrap_or(propagate);
        if !propagate && result.previous_context.is_none() {
            result.previous_context = Some(Box::new(active_ctx.clone()));
        }
        let local_ctx = match local_ctx {
            serde_json::Value::Array(x) => x,
            x => vec![ x ],
        };
        for ctx in local_ctx {
            match ctx {
                serde_json::Value::Null => {
                    if !override_protected && active_ctx.contains_protected_term_defintions() {
                        return Err(ContextProcessingError::InvalidContextNullification);
                    }

                    result = Context {
                        term_defs: HashMap::new(),
                        base_iri: active_ctx.original_base_url.into(),
                        original_base_url: active_ctx.original_base_url,
                        vocabulary_mapping: ExpandedIri::Null,
                        default_language: None,
                        default_base_direction: None,
                        previous_context: if !propagate { Some(Box::new(result.clone())) } else { None },
                    };
                },
                serde_json::Value::String(ref string) => {
                    let context = if let Ok(iri_ref) = IriRef::new(string) {
                        iri_ref.resolved(base_url.as_iri())
                    } else {
                        base_url.clone()
                    };

                    if remote_ctxs.len() > MAX_REMOTE_CONTEXTS {
                        return Err(ContextProcessingError::ContextOverflow);
                    }
                    remote_ctxs.push(context.clone());

                    let context_doc = load_document(context.as_str()).await?;
                    if let Some(c) = context_doc.get("@context") {
                        let loaded_context = c;
                        result = context_processing(
                            result,
                            loaded_context.clone(),
                            context.clone(),
                            Some(remote_ctxs.clone()),
                            Some(override_protected),
                            Some(propagate),
                        ).await?;
                    } else {
                        return Err(ContextProcessingError::InvalidRemoteContext);
                    }
                },
                serde_json::Value::Object(obj) => {
                    let mut obj = obj.clone();
                    if let Some(ver) = obj["@version"].as_str() {
                        if &ver[..] != "1.1" {
                            return Err(ContextProcessingError::InvalidVersion);
                        }
                    }

                    if obj.contains_key("@import") {
                        if let Some(imp) = obj["@import"].as_str() {
                            let import = IriRef::new(imp)
                                .map_err(|_| ContextProcessingError::InvalidImport)?
                                .resolved(base_url.as_iri());
                            let import_doc = load_document(import.as_str()).await?;
                            if let Some(imported_ctx) = import_doc["@context"].as_object() {
                                if imported_ctx.contains_key("@import") {
                                    return Err(ContextProcessingError::InvalidContextEntry);
                                }
                                obj = merge_maps(obj, imported_ctx.clone());
                            } else {
                                return Err(ContextProcessingError::InvalidRemoteContext);
                            }
                        } else {
                            return Err(ContextProcessingError::InvalidImport);
                        }
                    }

                    if obj.contains_key("@base") && remote_ctxs.is_empty() {
                        match obj["@base"] {
                            serde_json::Value::String(ref s) => {
                                if let Some(iri) = IriRef::new(&s).ok().map(|x| x.resolved(result.base_iri.as_iri()) ) {
                                    result.base_iri = iri;
                                } else {
                                    return Err(ContextProcessingError::InvalidBaseIri);
                                }
                            },
                            serde_json::Value::Null => {
                                // TODO: delete result.base_iri in some way
                            },
                            _ => return Err(ContextProcessingError::InvalidBaseIri)
                        }
                    }

                    if obj.contains_key("@vocab") {
                        match expand_iri(&mut result, obj["@vocab"], Some(true), None, None, None) {
                            res @ ExpandedIri::Null |
                            res @ ExpandedIri::BlankNodeIdent(_) |
                            res @ ExpandedIri::Iri(_) => {
                                result.vocabulary_mapping = res;
                            },
                            _ => return Err(ContextProcessingError::InvalidVocabMapping),
                        }
                    }

                    if obj.contains_key("@language") {
                        match obj["@language"] {
                            serde_json::Value::Null => {
                                result.default_language = None;
                            },
                            serde_json::Value::String(ref s) => {
                                // TODO check it looks like a language string (https://tools.ietf.org/html/bcp47)
                                result.default_language = Some(s.clone());
                            },
                            _ => return Err(ContextProcessingError::InvalidDefaultLanguage),
                        }
                    }

                    if obj.contains_key("@direction") {
                        match obj["@direction"] {
                            serde_json::Value::Null => {
                                result.default_base_direction = None;
                            },
                            serde_json::Value::String(ref s) => {
                                if let Some(dir) = s.parse().ok() {
                                    result.default_base_direction = Some(dir);
                                } else {
                                    return Err(ContextProcessingError::InvalidBaseDirection);
                                }
                            },
                            _ => return Err(ContextProcessingError::InvalidBaseDirection),
                        }
                    }

                    if obj.contains_key("@propagate") {
                        if obj["@propagate"].as_bool().is_none() {
                            return Err(ContextProcessingError::InvalidPropagate)
                        }
                    }

                    let mut defined = HashMap::new();
                    for (k, v) in obj.into_iter() {
                        if !k.starts_with('@') {
                            create_term_def(
                                &mut result,
                                obj,
                                k,
                                &mut defined,
                                Some(base_url),
                                obj["@protected"].as_bool(),
                                Some(override_protected),
                            );
                        }
                    }
                },
                _ => return Err(ContextProcessingError::InvalidLocalContext),
            }
        }
        Ok(result)
    }.boxed()
}

enum CreateTermError {
    CyclicIriMapping,
    KeywordRedefinition,
    InvalidTermDefinition,
    InvalidTypeMapping,
    InvalidReverseProperty,
    InvalidIriMapping,
    InvalidContainerMapping,
    InvalidScopedContext,
    InvalidLanguageMapping,
    ProtectedTermRedefinition,
    InvalidPrefixValue,
    InvalidNestValue,
    InvalidBaseDirection,
    InvalidKeywordAlias,
}

// https://www.w3.org/TR/json-ld11-api/#create-term-definition
fn create_term_def<'a>(
    active_ctx: &'a mut Context<'a>,
    local_ctx: serde_json::Map<String, serde_json::Value>,
    term: String,
    defined: &'a mut HashMap<String, bool>,
    base_url: Option<IriBuf>,
    protected: Option<bool>,
    override_protected: Option<bool>,
) -> BoxFuture<'a, Result<(), CreateTermError>> {
    async move {
        let override_protected = override_protected.unwrap_or(false);
        match defined.get(&term) {
            Some(true) => Ok(()),
            Some(false) => Err(CreateTermError::CyclicIriMapping),
            None => {
                defined.insert(term.clone(), false);
                let value = local_ctx[&term].clone();
                if &term == "@type" {
                    let valid = value.as_object()
                        .map(|v| {
                            v.keys().len() < 3 &&
                            !v.is_empty() &&
                            (v.get("@container").map(|c| c == "@set").unwrap_or(false) ||
                            v.get("@protected").is_some())
                        })
                        .unwrap_or(false);
                    if !valid {
                        return Err(CreateTermError::KeywordRedefinition);
                    }
                } else if term.starts_with('@') {
                    warn!("Tried to redefined keyword {}. Stopping term creation.", term);
                    return Err(CreateTermError::KeywordRedefinition);
                }

                let previous_def = active_ctx.term_defs.remove(&term);
                let (value, simple_term) = match value {
                    serde_json::Value::Null => ({
                        let mut map = serde_json::Map::new();
                        map.insert("@id".into(), serde_json::Value::Null);
                        map
                    }, false),
                    serde_json::Value::String(ref s) => ({
                        let mut map = serde_json::Map::new();
                        map.insert("@id".into(), serde_json::Value::String(s.clone()));
                        map
                    }, true),
                    serde_json::Value::Object(m) => (m, false),
                    _ => return Err(CreateTermError::InvalidTermDefinition)
                };
                let mut definition = TermDef {
                    iri: ExpandedIri::Null,
                    prefix: false,
                    protected: protected.unwrap_or(false),
                    reverse_property: false,
                    base_url: None,
                    context: None,
                    container_mapping: &[],
                    direction_mapping: None,
                    index_mapping: None,
                    language_mapping: None,
                    nest_value: None,
                    type_mapping: TypeMapping::None,
                };
                if value.contains_key("@type") {
                    let ty = match value["@type"] {
                        serde_json::Value::String(ref s) => s.clone(),
                        _ => return Err(CreateTermError::InvalidTypeMapping),
                    };
                    definition.type_mapping = match expand_iri(
                        &mut active_ctx,
                        serde_json::Value::String(ty),
                        None,
                        None,
                        Some(local_ctx.clone()),
                        Some(defined.clone())
                    ) {
                        ExpandedIri::Keyword(Keyword::Id) => TypeMapping::Id,
                        ExpandedIri::Keyword(Keyword::Json) => TypeMapping::Json,
                        ExpandedIri::Keyword(Keyword::None) => TypeMapping::None,
                        ExpandedIri::Keyword(Keyword::Vocab) => TypeMapping::Vocab,
                        ExpandedIri::Iri(i) => TypeMapping::Iri(i),
                        _ => return Err(CreateTermError::InvalidTypeMapping),
                    }
                }

                if value.contains_key("@reverse") {
                    if value.contains_key("@id") || value.contains_key("@nest") {
                        return Err(CreateTermError::InvalidReverseProperty);
                    }
                    if let Some(s) = value["@reverse"].as_str() {
                        if s.starts_with('@') {
                            warn!("@reverse value looking like a keyword: {}", s);
                            return Ok(());
                        }

                        match expand_iri(
                            &mut active_ctx,
                            serde_json::Value::String(s.to_owned()),
                            None,
                            None,
                            Some(local_ctx.clone()),
                            Some(defined.clone())
                        ) {
                            ExpandedIri::Null | ExpandedIri::Keyword(_) =>return Err(CreateTermError::InvalidIriMapping),
                            iri => { definition.iri = iri; }
                        }

                        if value.contains_key("@container") {
                            match value["@container"] {
                                serde_json::Value::String(ref s) if s == "@set" || s == "@index" => {
                                    definition.container_mapping = &[ &s ];
                                },
                                serde_json::Value::Null => {
                                    definition.container_mapping = &[];
                                },
                                _ => return Err(CreateTermError::InvalidReverseProperty),
                            }
                        }

                        definition.reverse_property = true;
                        active_ctx.term_defs.insert(term.clone(), definition);
                        defined.insert(term.clone(), true);
                    } else {
                        return Err(CreateTermError::InvalidReverseProperty);
                    }
                }

                if value.contains_key("@id") && value["@id"].as_str().map(|id| id != term).unwrap_or(false) {
                    match value["id"] {
                        serde_json::Value::Null => {},
                        serde_json::Value::String(ref s) => {
                            if keyword_like(s) {
                                warn!("@id in term definition looking like a keyword: {}", s);
                            }
                            match expand_iri(
                                &mut active_ctx,
                                serde_json::Value::String(s.clone()),
                                None,
                                None,
                                Some(local_ctx.clone()),
                                Some(defined.clone())
                            ) {
                                ExpandedIri::Null => return Err(CreateTermError::InvalidIriMapping),
                                ExpandedIri::Keyword(kw) if kw == Keyword::Context => {
                                    return Err(CreateTermError::InvalidKeywordAlias);
                                },
                                iri @ ExpandedIri::Iri(_) |
                                iri @ ExpandedIri::Keyword(_) |
                                iri @ ExpandedIri::BlankNodeIdent(_) => {
                                    definition.iri = iri;
                                }
                            }

                            if term[1..term.len() - 1].contains(':') || term.contains('/') {
                                defined.insert(term.clone(), true);
                                if expand_iri(&mut active_ctx, serde_json::Value::String(term), None, None, Some(local_ctx.clone()), Some(defined.clone())) != definition.iri {
                                    return Err(CreateTermError::InvalidIriMapping);
                                }
                            }
                        },
                        _ => return Err(CreateTermError::InvalidIriMapping)
                    }
                } else if term[1..].contains(':') {
                    let split = term.splitn(2, ':');
                    let prefix = split.next().unwrap();
                    let suffix = split.next().unwrap();
                    if local_ctx.contains_key(prefix) {
                        create_term_def(
                            &mut active_ctx,
                            local_ctx,
                            prefix.to_owned(),
                            &mut defined,
                            base_url,
                            protected,
                            Some(override_protected)
                        ).await?
                    }

                    if let Some(TermDef { iri: ExpandedIri::Iri(i), .. }) = active_ctx.term_defs.get(prefix) {
                        definition.iri = IriBuf::new(&(i.to_string() + suffix)).map(ExpandedIri::Iri).unwrap_or(ExpandedIri::Null);
                    } else {
                        // TODO: term might be a blank node identifier
                        definition.iri = IriBuf::new(&term).map(ExpandedIri::Iri).unwrap_or(ExpandedIri::Null);
                    }
                } else if term.contains('/') {
                    if let ExpandedIri::Iri(i) = expand_iri(&mut active_ctx, serde_json::Value::Object(local_ctx), None, None, None, None) {
                        definition.iri = ExpandedIri::Iri(i);
                    } else {
                        return Err(CreateTermError::InvalidIriMapping);
                    }
                } else if term == "@type" {
                    definition.iri = ExpandedIri::Keyword(Keyword::Type);
                } else if let ExpandedIri::Iri(voc) = active_ctx.vocabulary_mapping {
                    definition.iri = IriBuf::new(&(voc.to_string() + &term)).map(ExpandedIri::Iri).unwrap_or(ExpandedIri::Null);
                } else {
                    return Err(CreateTermError::InvalidIriMapping)
                }

                if let Some(cont) = value.get("@container") {
                    let expected_keyword = |k| [
                        "@graph",
                        "@id",
                        "@index",
                        "@language",
                        "@list",
                        "@set",
                        "@type"
                    ].contains(k);
                    definition.container_mapping = match cont {
                        serde_json::Value::Array(arr) if arr.len() == 1 => {
                            match arr[0] {
                                serde_json::Value::String(s) if expected_keyword(&s.as_str()) => &[ &s ],
                                _ => return Err(CreateTermError::InvalidContainerMapping)
                            }
                        },
                        serde_json::Value::Array(arr) if arr.len() == 2 || arr.len() == 3 => {
                            let mut found_id_or_index = false;
                            for elt in arr {
                                match elt {
                                    serde_json::Value::String(ref s) => if s == "@id" || s == "@index" {
                                        if found_id_or_index {
                                            return Err(CreateTermError::InvalidContainerMapping);
                                        }
                                        found_id_or_index = true;
                                    } else if s != "@set" && s != "@graph" {
                                        return Err(CreateTermError::InvalidContainerMapping);
                                    },
                                    _ => return Err(CreateTermError::InvalidContainerMapping),
                                }
                            }

                            let arr: Vec<_> = arr.iter().filter_map(serde_json::Value::as_str).collect();
                            if !found_id_or_index {
                                if arr.contains(&"@set") {
                                    let others = arr.iter().filter(|x| **x != "@set");
                                    if let Some(o) = others.next() {
                                        if !["@index", "@graph", "@id", "@type", "@language"].contains(o) || others.next().is_some() {
                                            return Err(CreateTermError::InvalidContainerMapping);
                                        }
                                    } else {
                                        return Err(CreateTermError::InvalidContainerMapping);
                                    }
                                } else {
                                    return Err(CreateTermError::InvalidContainerMapping);
                                }
                            }

                            &arr
                        },
                        serde_json::Value::String(s) if expected_keyword(&s.as_str()) => {
                            &[ &*s ]
                        },
                        _ => return Err(CreateTermError::InvalidContainerMapping)
                    };

                    if definition.container_mapping.contains(&"@type") {
                        match definition.type_mapping {
                            TypeMapping::None => {
                                definition.type_mapping = TypeMapping::Id;    
                            },
                            TypeMapping::Id | TypeMapping::Vocab => {},
                            _ => return Err(CreateTermError::InvalidTypeMapping),
                        }
                    }
                }

                if value.contains_key("@index") {
                    if !definition.container_mapping.contains(&"@index") {
                        return Err(CreateTermError::InvalidTermDefinition);
                    }

                    if let Some(iri) = value["@index"].as_str().and_then(|i| IriBuf::new(i).ok()) {
                        definition.index_mapping = Some(iri);
                    } else {
                        return Err(CreateTermError::InvalidTermDefinition);
                    }
                }

                if value.contains_key("@context") {
                    if context_processing(*active_ctx, value["@context"], base_url.expect("TODO"), None, Some(true), None).await.is_ok() {
                        local_ctx = value["@context"].as_object().unwrap().clone();
                    } else {
                        return Err(CreateTermError::InvalidScopedContext)
                    }
                }

                if value.contains_key("@language") && !value.contains_key("@type") {
                    match value["@language"] {
                        serde_json::Value::String(s) => { definition.language_mapping = Some(&s); },
                        serde_json::Value::Null => { definition.language_mapping = None; },
                        _ => return Err(CreateTermError::InvalidLanguageMapping),
                    }
                }

                if value.contains_key("@direction") && !value.contains_key("@type") {
                    if let Some(dir) = value["@direction"].as_str().and_then(|d| Direction::from_str(d).ok()) {
                        definition.direction_mapping = Some(dir);
                    } else {
                        return Err(CreateTermError::InvalidBaseDirection);
                    }
                }

                if value.contains_key("@nest") {
                    if let Some(s) = value["@nest"].as_str() {
                        if keyword_like(s) && s != "@nest" {
                            return Err(CreateTermError::InvalidNestValue);
                        }

                        definition.nest_value = Some(s);
                    } else {
                        return Err(CreateTermError::InvalidNestValue);
                    }
                }

                if value.contains_key("@prefix") {
                    if term.contains(':') || term.contains('/') {
                        return Err(CreateTermError::InvalidTermDefinition);
                    }

                    if let Some(p) = value["@prefix"].as_bool() {
                        definition.prefix = p;
                    } else {
                        return Err(CreateTermError::InvalidPrefixValue);
                    }
                }

                if value.keys()
                .filter(|x| ![
                    "@id",
                    "@reverse",
                    "@container",
                    "@context",
                    "@direction",
                    "@index",
                    "@language",
                    "@nest",
                    "@prefix",
                    "@protected",
                    "@type"
                ].contains(&x.as_str())).count() > 0 {
                    return Err(CreateTermError::InvalidTermDefinition);
                }

                if !override_protected && previous_def.map(|p| p.protected).unwrap_or(false) {
                    return Err(CreateTermError::ProtectedTermRedefinition);
                }

                active_ctx.term_defs.insert(term, definition);
                defined.insert(term, true);

                Ok(())
            }
        }    
    }.boxed()
}

// https://www.w3.org/TR/json-ld11-api/#iri-expansion
fn expand_iri<'a>(
    active_ctx: &'a mut Context<'a>,
    to_expand: serde_json::Value,
    document_relative: Option<bool>,
    vocab: Option<bool>,
    local_ctx: Option<serde_json::Map<String, serde_json::Value>>,
    defined: Option<HashMap<String, bool>>,
) -> ExpandedIri {
    let vocab = vocab.unwrap_or(false);
    let document_relative = document_relative.unwrap_or(false);
    match to_expand {
        serde_json::Value::Null => ExpandedIri::Null,
        serde_json::Value::String(ref value) => if let Ok(kw) = Keyword::from_str(&value) {
            ExpandedIri::Keyword(kw)
        } else {
            if keyword_like(&value) {
                warn!("Invalid keyword-like value");
                return ExpandedIri::Null;
            } else {
                if let Some(local_ctx) = local_ctx {
                    if let Some(entry) = local_ctx.get(value) {
                        if !defined.unwrap_or_default().get(value).unwrap_or(&false) {
                            create_term_def(
                                active_ctx,
                                local_ctx,
                                *value,
                                &mut defined.unwrap_or_default(),
                                None,
                                None,
                                None,
                            );
                        }
                    }
                }

                if let Some(TermDef { iri: ExpandedIri::Keyword(kw), .. }) = active_ctx.term_defs.get(value) {
                    return ExpandedIri::Keyword(*kw);
                }

                if vocab {
                    if let Some(TermDef { iri: i, .. }) = active_ctx.term_defs.get(value) {
                        return *i;
                    }
                }

                if value[1..].contains(':') {
                    let split = value.splitn(2, ':');
                    let prefix = split.next().unwrap();
                    let suffix = split.next().unwrap();
                    if suffix.starts_with("//") {
                        return IriBuf::new(value).map(ExpandedIri::Iri).unwrap_or(ExpandedIri::Null);
                    } else if prefix == "_" {
                        return ExpandedIri::BlankNodeIdent(suffix.to_owned());
                    } else {
                        if local_ctx.map(|l| l.contains_key(prefix)).unwrap_or(false) &&
                        !defined.and_then(|d| d.get(prefix)).unwrap_or(&false) {
                            create_term_def(
                                &mut active_ctx,
                                local_ctx.unwrap_or_default(),
                                prefix.to_owned(),
                                &mut defined.unwrap_or_default(),
                                None,
                                None,
                                None,
                            );
                        }

                        if let Some(def) = active_ctx.term_defs.get(prefix) {
                            if !def.iri.is_null() && def.prefix {
                                return def.iri.into_iri().and_then(|pref_iri| {
                                        IriBuf::new(&(pref_iri.to_string() + suffix)).ok()
                                    })
                                    .map(ExpandedIri::Iri)
                                    .unwrap_or(ExpandedIri::Null);
                            }
                        }
                    }
                }

                if vocab {
                    if let Some(voc) = active_ctx.vocabulary_mapping.to_string() {
                        return IriBuf::new(&(voc + value)).map(ExpandedIri::Iri).unwrap_or(ExpandedIri::Null);
                    }
                }

                if document_relative {
                    // TODO: this may do more than the "basic algorithm in section 5.2"
                    return IriRef::new(value)
                        .map(|i| ExpandedIri::Iri(i.resolved(&active_ctx.base_iri)))
                        .unwrap_or(ExpandedIri::Null);
                }
            }
                return IriBuf::new(value).map(ExpandedIri::Iri).unwrap_or(ExpandedIri::Null);
        },
        _ => ExpandedIri::Null,
    }
}

#[derive(Clone, PartialEq)]
enum ExpandedIri {
    Null,
    Keyword(Keyword),
    BlankNodeIdent(String),
    Iri(IriBuf),
}

impl ExpandedIri {
    fn is_null(&self) -> bool {
        match *self {
            ExpandedIri::Null => true,
            _ => false,
        }
    }

    fn into_iri(self) -> Option<IriBuf> {
        match self {
            ExpandedIri::Iri(i) => Some(i),
            _ => None,
        }
    }

    fn to_string(self) -> Option<String> {
        match self {
            ExpandedIri::Null => None,
            ExpandedIri::Keyword(k) => Some(k.to_string()),
            ExpandedIri::BlankNodeIdent(i) => Some(format!("_:{}", i)), // TODO: check they actually have this format
            ExpandedIri::Iri(iri) => Some(iri.to_string())
        }
    }
}

fn keyword_like(s: &str) -> bool {
    let mut chars = s.chars();
    chars.next().map(|c| c == '@').unwrap_or(false) &&
    chars.next().map(|c| c.is_alphabetic()).unwrap_or(false)
}

fn is_keyword(s: &str) -> bool {
    Keyword::from_str(s).is_ok()
}

/// All the JSON-LD keywords, as defined in [the
/// specification](https://www.w3.org/TR/json-ld11/#syntax-tokens-and-keywords).
///
/// Keywords are prefixed by a "@" sign in JSON-LD. For instance, `Keyword::Container`
/// is written `"@container"` in JSON-LD.
#[derive(Clone, PartialEq)]
enum Keyword {
    /// Used to set the base IRI against which to resolve those relative
    /// IRI references which are otherwise interpreted relative to the document.
    Base,
    /// Used to set the default container type for a term.
    Container,
    /// Used to define the short-hand names that are used throughout a JSON-LD document.
    /// These short-hand names are called terms and help developers to express
    /// specific identifiers in a compact manner.
    Context,
    /// Used to set the base direction of a JSON-LD value, which are not typed
    /// values (e.g. strings, or language-tagged strings).
    Direction,
    /// Used to express a graph.
    Graph,
    /// Used to uniquely identify node objects that are being described in the document with IRIs
    /// or blank node identifiers. A node reference is a node object containing only the @id property,
    /// which may represent a reference to a node object found elsewhere in the document.
    Id,
    /// Used in a context definition to load an external context within which the containing context
    /// definition is merged. This can be useful to add JSON-LD 1.1 features to JSON-LD 1.0 contexts.
    Import,
    /// Used in a top-level node object to define an included block, for including secondary
    /// node objects within another node object. 
    Included,
    /// Used to specify that a container is used to index information and that
    /// processing should continue deeper into a JSON data structure.
    Index,
    /// Used as the @type value of a JSON literal.
    Json,
    /// Used to specify the language for a particular string value or the default language of a JSON-LD document.
    Language,
    /// Used to express an ordered set of data.
    List,
    /// Used to define a property of a node object that groups together properties of
    /// that node, but is not an edge in the graph.
    Nest,
    /// Used as an index value in an index map, id map, language map, type map, or elsewhere where
    /// a map is used to index into other values, when the indexed node does not have the feature
    /// being indexed.
    None,
    /// With the value true, allows this term to be used to construct a compact IRI when compacting.
    Prefix,
    /// Used in a context definition to change the scope of that context. By default,
    /// it is true, meaning that contexts propagate across node objects (other than
    /// for type-scoped contexts, which default to false). Setting this to false causes
    /// term definitions created within that context to be removed when entering a new node object.
    Propagate,
    /// Used to prevent term definitions of a context to be overridden by other contexts.
    Protected,
    /// Used to express reverse properties.
    Reverse,
    /// Used to express an unordered set of data and to ensure that values are always represented
    /// as arrays.
    Set,
    /// Used to set the type of a node or the datatype of a typed value.
    Type,
    /// Used to specify the data that is associated with a particular property in the graph.
    Value,
    /// Used in a context definition to set the processing mode.
    Version,
    /// Used to expand properties and values in @type with a common prefix IRI.
    Vocab,
}

impl FromStr for Keyword {
    type Err = ();
    fn from_str(s: &str) -> Result<Keyword, ()> {
        match s {
            "@base" => Ok(Keyword::Base),
            "@container" => Ok(Keyword::Base),
            "@context" => Ok(Keyword::Base),
            "@direction" => Ok(Keyword::Direction),
            "@graph" => Ok(Keyword::Graph),
            "@id" => Ok(Keyword::Id),
            "@import" => Ok(Keyword::Import),
            "@included" => Ok(Keyword::Included),
            "@index" => Ok(Keyword::Index),
            "@json" => Ok(Keyword::Json),
            "@language" => Ok(Keyword::Language),
            "@list" => Ok(Keyword::List),
            "@nest" => Ok(Keyword::List),
            "@none" => Ok(Keyword::None),
            "@prefix" => Ok(Keyword::Prefix),
            "@propagate" => Ok(Keyword::Propagate),
            "@protected" => Ok(Keyword::Protected),
            "@reverse" => Ok(Keyword::Reverse),
            "@set" => Ok(Keyword::Set),
            "@type" => Ok(Keyword::Type),
            "@value" => Ok(Keyword::Value),
            "@version" => Ok(Keyword::Version),
            "@vocab" => Ok(Keyword::Vocab),
            _ => Err(()),
        }
    }
}

impl ToString for Keyword {
    fn to_string(&self) -> String {
        match *self {
            Keyword::Base => "@base",
            Keyword::Container => "@container",
            Keyword::Context => "@context",
            Keyword::Direction => "@direction",
            Keyword::Graph => "@graph",
            Keyword::Id => "@id",
            Keyword::Import => "@import",
            Keyword::Included => "@included",
            Keyword::Index => "@index",
            Keyword::Json => "@json",
            Keyword::Language => "@language",
            Keyword::List => "@list",
            Keyword::Nest => "@nest",
            Keyword::None => "@none",
            Keyword::Prefix => "@prefix",
            Keyword::Propagate => "@propagate",
            Keyword::Protected => "@protected",
            Keyword::Reverse => "@reverse",
            Keyword::Set => "@set",
            Keyword::Type => "@type",
            Keyword::Value => "@value",
            Keyword::Version => "@version",
            Keyword::Vocab => "@vocab",
        }.to_owned()
    }
}

fn merge_maps(
    mut map1: serde_json::Map<String, serde_json::Value>,
    map2: serde_json::Map<String, serde_json::Value>,
) -> serde_json::Map<String, serde_json::Value> {
    for (k, v) in map2.into_iter() {
        if !map1.contains_key(&k) {
            map1[&k] = v;
        }
    }
    map1
}

/*
impl<'a> Default for Context<'a> {
    fn default() -> Self {
        Context {
            language: "und",
            cont: HashMap::new(),
        }
    }
}

impl<'a> std::ops::Add for Context<'a> {
    type Output = Self;
    fn add(self, other: Self) -> Self {
        let mut cont = self.cont;
        for (k, v) in other.cont {
            if !cont.has(k) {
                cont.insert(k, v);
            }
        }
        Context {
            language: self.language,
            cont,
        }
    }
}

impl<'a> Context<'a> {
    async fn build(json: &serde_json::Value) -> Context<'a> {
        match *json {
            serde_json::Value::String(ref s) => Self::from_url(&s),
            serde_json::Value::Array(ref arr) => {
                let mut ctx = None;
                for c in arr {
                    ctx = Some(match ctx {
                        None => Context::build(c).await,
                        Some(c2) => c2 + Context::build(c).await,
                    });
                }

                if let Some(c) = ctx {
                    c
                } else {
                    Context::default()
                }
            },
            serde_json::Value::Object(ref obj) => {
                for (k, v) in obj {

                }
            }
        }
    }
}*/
