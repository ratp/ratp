use actix_web::{web::{Data, Form}, Responder, HttpResponse};
use actix_identity::Identity;
use indradb::{RocksdbDatastore, Type, Vertex, Datastore, Transaction, RangeVertexQuery, VertexQueryExt, SpecificVertexQuery};
use serde_json::Value as JsonValue;
use serde::Deserialize;
use std::sync::Arc;

#[derive(Debug)]
pub struct User {
    pub email: String,
    hashed_password: String,
    uuid: uuid::Uuid,
}

impl User {
    pub fn get(db: &RocksdbDatastore, id: uuid::Uuid) -> Option<User> {
        let trans = db.transaction().ok()?;
        let mut props = trans.get_all_vertex_properties(SpecificVertexQuery::single(id)).ok()?.into_iter().next()?.props.into_iter();
        Some(User {
            email: props.find(|x| x.name == "email")?.value.as_str()?.to_owned(),
            hashed_password: props.find(|x| x.name == "password")?.value.as_str()?.to_owned(),
            uuid: id,
        })
    }

    pub fn get_by_email(db: &RocksdbDatastore, email_to_find: String) -> Option<User> {
        let trans = db.transaction().ok()?;
        let emails = trans.get_vertex_properties(RangeVertexQuery::new(std::u32::MAX).property("email")).ok()?;
        let email_id = emails.into_iter()
            .find(|x| x.value == JsonValue::String(email_to_find.clone()))?
            .id;
        let mut props = trans.get_all_vertex_properties(SpecificVertexQuery::single(email_id)).ok()?
            .into_iter().next()?.props.into_iter();
        Some(User {
            email: props.find(|x| x.name == "email")?.value.as_str()?.to_owned(),
            hashed_password: props.find(|x| x.name == "password")?.value.as_str()?.to_owned(),
            uuid: email_id,
        })
    }

    pub fn register(db: &RocksdbDatastore, email: String, password: String) -> Option<User> {
        let trans = db.transaction().ok()?;
        let cost = 10;
        let hashed_password = bcrypt::hash(&password, cost).ok()?;
        let t = Type::new("ratp-user").ok()?;
        let vert = Vertex::new(t);
        trans.create_vertex(&vert).ok()?;
        trans.set_vertex_properties(
            SpecificVertexQuery::single(vert.id).property("email"),
            &JsonValue::String(email.clone())
        ).ok()?;
        trans.set_vertex_properties(
            SpecificVertexQuery::single(vert.id).property("password"),
            &JsonValue::String(hashed_password.clone())
        ).ok()?;
        Some(User {
            uuid: vert.id,
            email,
            hashed_password,
        })
    }

    pub fn login(db: &RocksdbDatastore, email: String, password: String) -> Option<User> {
        User::get_by_email(&db, email).and_then(|u| bcrypt::verify(&password, &u.hashed_password).ok().and_then(|r| if r { Some(u) } else { None }))
    }
}

pub async fn register_page() -> impl Responder {
    HttpResponse::Ok()
        .header("Content-Type", "text/html; charset=utf-8")
        .body(
r#"<!doctype HTML>
<html>
    <head>
        <meta charset="utf8">
        <title>Register</title>
    </head>
    <body>
        <form method="POST">
            <label for="email">Email</label>
            <input id="email" name="email" type="email">

            <label for="password">Password</label>
            <input id="password" name="password" type="password">

            <input type="submit" value="Register">
        </form>
    </body>
</html>
"#)
}

#[derive(Deserialize)]
pub struct RegisterForm {
    email: String,
    password: String,
}

pub async fn register(db: Data<Arc<RocksdbDatastore>>, form: Form<RegisterForm>) -> impl Responder {
    // TODO: validation
    User::register(&db, form.email.clone(), form.password.clone());
    HttpResponse::SeeOther()
        .header("Location", "/login")
        .finish()
}

pub async fn login_page() -> impl Responder {
    HttpResponse::Ok()
        .header("Content-Type", "text/html; charset=utf-8")
        .body(
r#"<!doctype HTML>
<html>
    <head>
        <meta charset="utf8">
        <title>Login</title>
    </head>
    <body>
        <form method="POST">
            <label for="email">Email</label>
            <input id="email" name="email" type="email">

            <label for="password">Password</label>
            <input id="password" name="password" type="password">

            <input type="submit" value="Login">
        </form>
    </body>
</html>
"#)
}

pub async fn login(db: Data<Arc<RocksdbDatastore>>, form: Form<RegisterForm>, id: Identity) -> impl Responder {
    if let Some(user) = User::login(&db, form.email.clone(), form.password.clone()) {
        id.remember(user.uuid.to_string());
        HttpResponse::SeeOther()
            .header("Location", "/")
            .finish()
    } else {
        HttpResponse::SeeOther()
            .header("Location", "/login")
            .finish()
    }
}
