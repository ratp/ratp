#[macro_use] extern crate log;
use actix_web::{web, App, HttpResponse, HttpServer, Responder};
use std::sync::Arc;
use actix_identity::{IdentityService, Identity, CookieIdentityPolicy};

mod config;
mod federation;
mod plugins;
mod users;

async fn index(id: Identity, db: web::Data<Arc<indradb::RocksdbDatastore>>) -> impl Responder {
    info!("index");
    if let Some(user) = id.identity().and_then(|id| users::User::get(&db, uuid::Uuid::parse_str(&id).unwrap())) {
        HttpResponse::Ok().body(format!("Hello {}!", user.email))
    } else {
        HttpResponse::Ok().body("Hello world!")
    }
}

#[actix_rt::main]
async fn main() -> std::io::Result<()> {
    let config_file = async_std::fs::read_to_string("ratp.toml").await.expect("Config reading error");
    let conf: config::Config = toml::from_str(&config_file).expect("Config parsing error");

    pretty_env_logger::formatted_timed_builder()
    .filter_level(match &conf.log_level[..] {
        "debug" => log::LevelFilter::Debug,
        "warn" => log::LevelFilter::Warn,
        "trace" => log::LevelFilter::Trace,
        "error" => log::LevelFilter::Error,
        "info" => log::LevelFilter::Info,
        _ => log::LevelFilter::Off,
    })
    .init();

    for app in conf.app {
        let plug = plugins::Plugin::load(&app.name).await;
        info!("Loaded {:?} v{:?}", plug.name, plug.version());
    }

    info!("Database setup…");
    let db = indradb::RocksdbDatastore::new("ratp.db", Some(512), true)
        .expect("Expected to be able to create the RocksDB datastore");
    federation::save(&db, "https://soc.punktrash.club/users/Tugliffe").await;

    let db_arc = Arc::new(db);
    info!("Starting server on {}", conf.listen);
    HttpServer::new(move || {
        let db_clone = db_arc.clone();
        App::new()
            .wrap(IdentityService::new(CookieIdentityPolicy::new(&[0; 32]) // TODO: generate a real key
                    .name("auth")
                    .secure(false)))
            .data(db_clone)
            .route("/", web::get().to(index))
            .service(
                web::resource("/register")
                    .route(web::get().to(users::register_page))
                    .route(web::post().to(users::register))
            )
            .service(
                web::resource("/login")
                    .route(web::get().to(users::login_page))
                    .route(web::post().to(users::login))
            )
            .route("/federation/{id}", web::get().to(federation::get))
    })
    .bind(conf.listen)?
    .run()
    .await
}
