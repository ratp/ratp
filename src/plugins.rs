use async_std::{fs, path::Path};
use wasmtime::*;
use wasmtime_wasi::{Wasi, WasiCtx};
// use wasmtime_interface_types::ModuleData;

pub struct Plugin<'a> {
    pub name: &'a str,
    runtime: Instance,
    // data: ModuleData,
}

impl<'a> Plugin<'a> {
    pub async fn load(name: &'a str) -> Plugin<'a> {
        let path = Path::new(".").join("plugins").join(name).join("plugin.wasm");
        let wasm = fs::read(path).await.unwrap();

        let engine = Engine::new(Config::new().wasm_multi_value(true));
        let store = Store::new(&engine);
        // let data = ModuleData::new(&wasm).unwrap();
        let module = Module::from_binary(&store, &wasm).unwrap();
        let wasi = Wasi::new(&store, WasiCtx::new(std::env::args()).unwrap());
        let mut imports = Vec::new();
        for import in module.imports() {
            if import.module() == "wasi_snapshot_preview1" {
                if let Some(export) = wasi.get_export(import.name()) {
                    imports.push(Extern::from(export.clone()));
                    continue;
                }
            }
            panic!(
                "couldn't find import for `{}::{}`",
                import.module(),
                import.name()
            );
        }

        let instance = Instance::new(&module, &imports).unwrap();

        Plugin {
            runtime: instance,
            // data,
            name,
        }
    }

    pub fn call_i32(&self, name: &str) -> i32 {
        self.runtime.get_export(name)
            .and_then(|e| e.func())
            .and_then(|f| f.call(&[]).ok())
            .unwrap()[0]
            .unwrap_i32()
    }

    pub fn version(&self) -> (i32, i32, i32) {
        (
            self.call_i32("version_major"),
            self.call_i32("version_minor"),
            self.call_i32("version_patch"),
        )
    }
}
