use serde::Deserialize;

#[derive(Deserialize)]
#[serde(rename_all = "kebab-case")]
pub struct Config {
    pub domain: String,
    pub auto_https: bool,
    pub log_level: String,
    #[serde(default = "default_address")]
    pub listen: String,
    pub app: Vec<App>
}

#[derive(Deserialize)]
#[serde(rename_all = "kebab-case")]
pub struct App {
    pub name: String,
}

fn default_address() -> String {
    "127.0.0.1:7878".to_string()
}
